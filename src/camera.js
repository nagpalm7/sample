import {CameraKitCamera} from 'react-native-camera-kit'
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Button} from 'react-native';

export default class camera extends Component<Props> {
    render() {
        return (
        <CameraKitCamera
            ref={cam => this.camera = cam}
            style={{
              flex: 1,
              backgroundColor: 'white'
            }}
            cameraOptions={{
              flashMode: 'auto',            
              focusMode: 'on',         
              zoomMode: 'on',          
              ratioOverlay:'16:9',            
              ratioOverlayColor: '#00000077'
            }}
            onReadQRCode={(event) => console.log(event.nativeEvent.qrcodeStringValue)}
          />
        );
      }
    }
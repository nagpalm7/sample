import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Button} from 'react-native';
import DatePicker from './src/datePicker'
import Camera from './src/camera'

type Props = {};
export default class App extends Component<Props> {
  constructor(props){
    super(props);
    this.state={
      date:false,
      camera:false
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <Button onPress={()=>this.setState({date:!this.state.date})} title='Date Picker'/>
        <Button onPress={()=>this.setState({camera:!this.state.camera})} title='Camera'/>
        {this.state.date && <DatePicker/>}
        {this.state.camera && <Camera/>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
